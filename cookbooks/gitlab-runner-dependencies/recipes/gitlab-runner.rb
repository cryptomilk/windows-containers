# Installation taken from https://docs.gitlab.com/runner/install/windows.html
directory 'Create root directory' do
  path node[:gitlab_runner][:root_path]
  action :create
end

remote_file 'Download gitlab-runner from S3' do
  path node[:gitlab_runner][:exe_path]
  source "https://gitlab-runner-downloads.s3.amazonaws.com/#{node[:gitlab_runner][:version]}/binaries/gitlab-runner-windows-amd64.exe"
  checksum node[:gitlab_runner][:checksum]
  action :create
end

windows_path 'Add GitLab Runner to path' do
  path node[:gitlab_runner][:root_path]
  action :add
end

